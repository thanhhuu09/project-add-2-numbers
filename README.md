MyBigNumber - Cách chạy code trong Command Prompt (CMD)

- Yêu cầu hệ thống
+ Node.js đã được cài đặt trên máy tính.

Cách chạy:
+ Sao chép đoạn code MyBigNumber vào một tệp tin với tên MyBigNumber.js.
+ Mở Command Prompt (CMD).
+ Di chuyển đến thư mục chứa tệp tin MyBigNumber.js bằng cách sử dụng lệnh cd (change directory). Ví dụ:
    cd C:\path\to\folder
+ Chạy lệnh sau để thực thi đoạn code:
    node MyBigNumber.js
=> Kết quả phép toán sẽ được hiển thị trên Command Prompt.
