class MyBigNumber {
  static sum(stn1, stn2) {
    const len1 = stn1.length; // Độ dài chuỗi 1
    const len2 = stn2.length; // Độ dài chuỗi 2
    const maxLen = Math.max(len1, len2);

    let result = "";
    let carry = 0; // Biến nhớ

    for (let i = 0; i < maxLen; i++) {
      let digit1, digit2;

      // Kiểm tra và lấy giá trị của chữ số từ chuỗi stn1
      if (i < len1) {
        digit1 = parseInt(stn1[len1 - 1 - i]);
      } else {
        digit1 = 0;
      }

      // Kiểm tra và lấy giá trị của chữ số từ chuỗi stn2
      if (i < len2) {
        digit2 = parseInt(stn2[len2 - 1 - i]);
      } else {
        digit2 = 0;
      }

      // Thực hiện phép cộng của hai chữ số và biến nhớ
      const sum = digit1 + digit2 + carry;
      carry = Math.floor(sum / 10); // Cập nhật giá trị biến nhớ
      result = (sum % 10) + result; // Thêm chữ số kết quả vào đầu chuỗi

      console.log(`Step ${i + 1}: ${digit1} + ${digit2} + ${carry} = ${sum}`);
    }

    if (carry > 0) {
      result = carry + result; // Nếu còn biến nhớ, thêm giá trị của biến nhớ vào đầu chuỗi kết quả
    }

    return result;
  }
}

const result = MyBigNumber.sum("1234", "897");
console.log(`Final result: ${result}`);
